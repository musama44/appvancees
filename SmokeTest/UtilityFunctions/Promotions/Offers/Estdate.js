function getDate() {
  var f = java.time.format.DateTimeFormatter.ofLocalizedDateTime( java.time.format.FormatStyle.FULL );
  f = f.withLocale( java.util.Locale.US )
  var z = java.time.ZoneId.of( "EST5EDT" );
  var zdt = java.time.ZonedDateTime.now( z );
 var dateval= java.time.format.DateTimeFormatter.ofPattern("MM/dd/yyyy hh:mm").format(zdt);
  log(dateval);
  return dateval;
}

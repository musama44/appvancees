
function adjust(POS_CORRELATION_ID) {
    var data = {   
    "currentCorrelationId": POS_CORRELATION_ID,
    "action": "adjustment",
    "channel": "POS",
    "transactionDate": isoDate,
    "retailBanner": "RXAL",
    "storeNumber": "0034",
    "tillNumber": "04",
    "employeeCode": "11120",
    "priceMatrix": "R",
    "reversalLineItems": [{
        "sku": "73385499157",
        "quantity": 1
    }]
};
    var call = createPost($POShost + "/client/sessions/" + POS_CORRELATION_ID + "/adjustments", data);
    call.addHeader("MCK-CORRELATIONID", POS_CORRELATION_ID);
    call.addHeader("MCK-MSGTIMESTAMP", isoDate);
    call.addHeader("MCK-APPID", "RXL-POS-1");
  	call.addHeader("Authorization", "Bearer " + POS_TOKEN);
    var resp = process(call);
    var json = JSON.parse(resp.getDataString());
  
};


// ACTUAL USE CASE
adjust(POS_CORRELATION_ID);
var F_isoDate = new Date().toISOString();

function Adjustment(POS_CORRELATION_ID) {
    var data = {
    "currentCorrelationId": RPOS_CORRELATION_ID,
    "action": "adjustment",
    "channel": "POS",
    "reservationToken": "",
    "transactionDate": F_isoDate,
    "retailBanner": "RXAL",
    "storeNumber": "2032",
    "tillNumber": "04",
    "employeeCode": "11120",
    "loyaltyId": $cardNumber,
    "priceMatrix": "R",
     "reversalLineItems": [
            {
                "sku": "77105811571",
                "quantity": 1,
    },{
                "sku": "77105811572",
                "quantity": 1,
    },
        ]
};
    var call = createPost($POShost + "/client/sessions/" + POS_CORRELATION_ID + "/adjustments", data);
    call.addHeader("MCK-CORRELATIONID", POS_CORRELATION_ID);
    call.addHeader("MCK-MSGTIMESTAMP", isoDate);
    call.addHeader("MCK-APPID", "RXL-POS-1");
  	call.addHeader("Authorization", "Bearer " + POS_TOKEN);
    var resp = process(call);
    var json = JSON.parse(resp.getDataString());
  
  	locker.put("Adj_Points",json.pointsAdjustment);
};


//setGlobalBearerToken();
Adjustment(POS_CORRELATION_ID);
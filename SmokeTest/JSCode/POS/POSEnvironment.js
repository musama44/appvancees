//
// Service Suite is a AppvanceIQ javascript powered API automation engine that allows for quick and 
// easy development of an API framework useful for functional, performace, load testing.
// Find more information in 
//     https://appvance.atlassian.net/wiki/spaces/CKB/pages/865239246/Service+Suite
//     https://appvance.atlassian.net/wiki/spaces/CKB/pages/865271937/List+of+Service+Suite+Calls

// can use include to organize your code into reusable blocks with common functions, endpoitns dicts, etc.
// example: include("{mds}/utils.js"); 

const POS_CLIENT_ID = "5ejn6259qm64mes9r5amfhlprm";
const POS_CLIENT_SECRET = "m8ak0jmk7eu0h57df8hcgf6nrbbn8e1ukrshop5nhmqn6cknslr";
const POS_OAUTH_ENABLED = true;
const POS_TOKEN_URL = "https://wizard-dev.auth.us-east-1.amazoncognito.com/oauth2/token";
var $POShost = "https://api2-us-east-1.stage.exchangesolutions.com/qa-mammoth-pos";

// Next variables can be data enabled for execution in IDE using the following code
// In scenario execution this readCSV wont get executed as the $accoundID variable 
// should get its value from the scenario Data Provider Library (DPL) assign to the
// service suite test case. But the following lines are useful to run the script in 
// the Service Suite and get the values from the .csv file instead of hard coding them.
//    if (typeof $accountID == "undefined") {
//       readCSV("{mds}/../data/myDataFile.csv",0) 
//    }

// If you do not want to hard code this values and use a .csv file use the lines above with
// the readCSV command.
// The following values for $accoundID and $cardNumber I got from the video shared in the slack channel

/**
 * processJSON
 * @param {HttpRequest} POST call 
 * @returns 
 */

/**
 * generateCorrelationId
 * Code for this function was copied and pasted from the provided sample Postman collection
 * @returns correlation id similar to "6690bf2a-fa23-4f05-b12b-6e0023f36ae2"
 */
function generateCorrelationId() {
    var dt = new Date().getTime();
    var correlation_id = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (dt + Math.random() * 16) % 16 | 0;
        dt = Math.floor(dt / 16);
        return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
    return correlation_id;
};
const POS_CORRELATION_ID = generateCorrelationId();

/**
 * Grabs the access_token from the server and set it as a global Bearer authentication header  
 * added automatically to all calls.
 */
function POS_setGlobalBearerToken() {
    const AUTHORIZATION = "Basic NWVqbjYyNTlxbTY0bWVzOXI1YW1maGxwcm06bThhazBqbWs3ZXUwaDU3ZGY4aGNnZjZucmJibjhlMXVrcnNob3A1bmhtcW42Y2tuc2xy";
    var data = formData();
    data.put("grant_type", "client_credentials");
    var call = createPost(POS_TOKEN_URL, data);
    call.addHeader("Authorization", AUTHORIZATION);
    var json = processJSON(call);
    POS_TOKEN = json.access_token;
   
};

POS_setGlobalBearerToken();
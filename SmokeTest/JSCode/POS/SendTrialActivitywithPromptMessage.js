
function TrialActivitywithPM(POS_CORRELATION_ID) {
   var data = { "action": "PURCHASE",
    "channel": "POS",
    "reservationToken": "",
    "transactionDate": isoDate,
    "retailBanner": "RXAL",
    "storeNumber": "2378",
    "tillNumber": "04",
    "employeeCode": "11120",
    "loyaltyId": $cardNumber,
    "priceMatrix": "R",
    //"callId": "{{call_id}}",
    "cart": {
        "saleLineItems": [
            {
                "subCategory": "57001",
                "sku": "07000085203",
                "quantity": 1,
                "originalSaleAmount": "4500",
                "saleAmount": "4500",
                "storeCoupon": "",
                "itemDiscount": "",
                "mnfCoupon": "",
                "promoCoupon": "",
                "itemTax": "0",
                "finalSaleAmount": "4868",
                "tags": []
            }
        ],
        "totalSaleAmount": "4998",
        "couponCodes": []
    },
    "tender": {
        "VISA": {
            "prefix": "898724",
            "amount": "1808",
            "idType": "LAST_4",
            "suffix": "8888"
        },
        "LOYALTY": "0"
    }
};
    var call = createPost($POShost + "/client/users/" + POS_CORRELATION_ID + "/activity/trial/v2", data);
    call.addHeader("MCK-CORRELATIONID", POS_CORRELATION_ID);
    call.addHeader("MCK-MSGTIMESTAMP", isoDate);
    call.addHeader("MCK-APPID", "RXL-POS-1");
  	call.addHeader("Authorization", "Bearer " + POS_TOKEN);
    var resp = process(call);
    var json = JSON.parse(resp.getDataString());
  locker.put("msg" , json.promptMessage);

};


//setGlobalBearerToken();
TrialActivitywithPM(POS_CORRELATION_ID);
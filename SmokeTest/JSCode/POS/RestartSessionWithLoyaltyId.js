function generateCorrelationId() {
    var dt = new Date().getTime();
    var correlation_id = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (dt + Math.random() * 16) % 16 | 0;
        dt = Math.floor(dt / 16);
        return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
    return correlation_id;
};
const RPOS_CORRELATION_ID = generateCorrelationId();
/**
 * startSessionWithLoyaltyId
 * Name of the function copied from Postman collection shared
 * @param {String} cardNumber 
 */
function startSessionWithLoyaltyId($cardNumber) {
    var data = {
        "correlationId": RPOS_CORRELATION_ID,
        "cardScanned": true
    };
    
    var call = createPost($POShost + "/client/users/" + $cardNumber + "/sessions", data);
    call.addHeader("MCK-CORRELATIONID", RPOS_CORRELATION_ID);
    call.addHeader("MCK-MSGTIMESTAMP", isoDate);
    call.addHeader("MCK-APPID", "RXL-POS-1");
  	call.addHeader("Authorization", "Bearer " + POS_TOKEN);
    var resp = process(call);
    var json = JSON.parse(resp.getDataString());
  var sid = json.sessionCorrelationId;
  locker.put("sessionCorrelationId" , sid);
    // Printout some values
    log("User Status: " + json.userStatus);
    log("Card Status: " + json.cardStatus);/*
    log("Is Valid: " + json.isValid);
		log("Available Points Balance: " + json.availablePointBalance);
  	log("loyaltyId: " + json.loyaltyId);
  	log("sessionStatus: " + json.sessionStatus);
  	log("sessionCorrelationId: " + json.sessionCorrelationId);
  	var a = json.sessionCorrelationId;

    // Can define assertions and validations like
    assertTrue(json.isValid);
    verifyEquals(json.cardStatus, "ACTIVE")*/
};


//locker.put("CORRELATION_ID",CORRELATION_ID);
//TrialActivity(CORRELATION_ID);
// ACTUAL USE CASE

startSessionWithLoyaltyId($cardNumber);
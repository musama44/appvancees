var F_isoDate = new Date().toISOString();

function FinalizeActivity(POS_CORRELATION_ID) {
    var data = {
    "action": "PURCHASE",
    "channel": "POS",
    "reservationToken": "",
    "transactionDate": F_isoDate,
    "retailBanner": "RXAL",
    "storeNumber": "2032",
    "tillNumber": "04",
    "employeeCode": "11120",
    "loyaltyId": $cardNumber,
    "priceMatrix": "R",
   // "callId": "{{call_id}}",
    "cart": {
        "saleLineItems": [
            {
                "subCategory": "57001",
                "sku": "77105811571",
                "quantity": 1,
                "originalSaleAmount": "1000",
                "saleAmount": "1000",
                "storeCoupon": "",
                "itemDiscount": "",
                "mnfCoupon": "",
                "promoCoupon": "",
                "itemTax": "130",
                "finalSaleAmount": "1000",
                "tags": []
            }
        ],
        "totalSaleAmount": "10000",
        "couponCodes": []
    },
    "tender": {
        "VISA": {
           // "prefix": "898724",
            "amount": "1130",
            "idType": "LAST_4",
            "suffix": "1204"
        },
        //"LOYALTY": "0",
      	//"MCOUPON": "12346"
    }
};
    var call = createPost($POShost + "/client/users/" + POS_CORRELATION_ID + "/activity/finalize", data);
    call.addHeader("MCK-CORRELATIONID", POS_CORRELATION_ID);
    call.addHeader("MCK-MSGTIMESTAMP", isoDate);
    call.addHeader("MCK-APPID", "RXL-POS-1");
  	call.addHeader("Authorization", "Bearer " + POS_TOKEN);
    var resp = process(call);
    var json = JSON.parse(resp.getDataString());
  
  	locker.put("EB_points" , json.pointsEarnedBase);
  	locker.put("TE_points" , json.pointsEarnedTotal);
  	locker.put("ET_points" , json.pointsEarnedTargeted);
  	//locker.put("P_Point",json.RBC_PARTNER_POINTS);
  	//locker.put("O_points",json.pointsEarned.offerRewards);
  
};


//setGlobalBearerToken();
FinalizeActivity(POS_CORRELATION_ID);
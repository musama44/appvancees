var F_isoDate = new Date().toISOString();
function FinalizeActivity(POS_CORRELATION_ID) {
    var data = {
    "action": "PURCHASE",
    "channel": "POS",
    "reservationToken": "",
    "transactionDate": F_isoDate,
    "retailBanner": "RXAL",
    "storeNumber": "2032",
    "tillNumber": "04",
    "employeeCode": "11120",
    "loyaltyId": $cardNumber,
    "priceMatrix": "R",
   // "callId": "{{call_id}}",
    "cart": {
        "saleLineItems": [
            {
                "subCategory": "57001",
                "sku": "77105811571",
                "quantity": 1,
                "originalSaleAmount": "10000",
                "saleAmount": "10000",
                "storeCoupon": "",
                "itemDiscount": "",
                "mnfCoupon": "",
                "promoCoupon": "",
                "itemTax": "1300",
                "finalSaleAmount": "10000",
                "tags": []
            },{
                  "subCategory": "57001",
                  "sku": "77105811572",
                  "quantity": "1",
                  "originalSaleAmount": "2000",
                  "storeCoupon": "",
                  "itemDiscount": "",
                  "mnfCoupon": "",
                  "promoCoupon": '{""}',
                  "itemTax": "130",
                  "saleAmount": "2000",
                  "finalSaleAmount": "2000",
                  "tags": '[]'
              }
        ],
        "totalSaleAmount": "12000",
        "couponCodes": []
    },
    "tender": {
      	"CASH": "6400",
        "VISA": [{
           // "prefix": "898724",
            "amount": "4000.00",
            "idType": "LAST_4",
            "suffix": "1204"
        },
          {
        
        "amount": "3000.00",
        "idType": "LAST_4",
        "linked": "RBC",
        "suffix": "1234"
      }],
        "LOYALTY": "0",

    }
};
    var call = createPost($POShost + "/client/users/" + POS_CORRELATION_ID + "/activity/finalize", data);
    call.addHeader("MCK-CORRELATIONID", POS_CORRELATION_ID);
    call.addHeader("MCK-MSGTIMESTAMP", F_isoDate);
    call.addHeader("MCK-APPID", "RXL-POS-1");
  	call.addHeader("Authorization", "Bearer " + POS_TOKEN);
    var resp = process(call);
    var json = JSON.parse(resp.getDataString());
  
  	locker.put("EB_points",json.pointsEarnedBase);
  	locker.put("TE_points",json.pointsEarnedTotal);
  	//locker.put("P_Point",json.RBC_PARTNER_POINTS);
  	//locker.put("O_points",json.pointsEarned.offerRewards);
  
};


//setGlobalBearerToken();
FinalizeActivity(POS_CORRELATION_ID);
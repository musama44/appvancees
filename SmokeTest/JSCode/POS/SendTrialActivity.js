
function TrialActivity(POS_CORRELATION_ID) {
    var data = {
    "action": "PURCHASE",
    "channel": "POS",
    "reservationToken": "",
    "transactionDate": isoDate,
    "retailBanner": "RXAL",
    "storeNumber": "2378",
    "tillNumber": "04",
    "employeeCode": "11120",
    "loyaltyId": $cardNumber,
    "priceMatrix": "R",
    //"callId": "{{call_id}}",
    "cart": {
        "saleLineItems": [
            {
                "subCategory": "44100",
                "sku": "73385499157",
                "quantity": 1,
                "originalSaleAmount": "4500",
                "saleAmount": "4500",
                "storeCoupon": "",
                "itemDiscount": "",
                "mnfCoupon": "",
                "promoCoupon": "",
                "itemTax": "0",
                "finalSaleAmount": "2000",
                "tags": []
            }
        ],
        "totalSaleAmount": "2260",
        "couponCodes": []
    },
    "tender": {
        "VISA": {
            "prefix": "898724",
            "amount": "2260",
            "idType": "LAST_4",
            "suffix": "8888"
        },
        "LOYALTY": "0"
    }
};
    var call = createPost($POShost + "/client/users/" + POS_CORRELATION_ID + "/activity/trial", data);
    call.addHeader("MCK-CORRELATIONID", POS_CORRELATION_ID);
    call.addHeader("MCK-MSGTIMESTAMP", isoDate);
  call.addHeader("Authorization", "Bearer " + POS_TOKEN);
    call.addHeader("MCK-APPID", "RXL-POS-1");
    var resp = process(call);
    var json = JSON.parse(resp.getDataString());

};

//setGlobalBearerToken();
//setGlobalBearerToken();
TrialActivity(POS_CORRELATION_ID);
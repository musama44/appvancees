function processJSON(postCall) {
    var resp = process(postCall);
    return JSON.parse(resp.getDataString());
};
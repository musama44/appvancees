// Initialize required objects

var kwargs = {
    "subCategory": "57001",
    "sku": "77105811571",
    "quantity": "1",
    "originalSaleAmount": "10000",
    "storeCoupon": "",
    "itemDiscount": "",
    "mnfCoupon": "",
    "promoCoupon": '{""}',
    "itemTax": "1300",
    "saleAmount": "10000",
    "finalSaleAmount": "10000",
    "tags": '["GWP DISCOUNT"]'
};

var dictCart = {};
dictCart['saleLineItems'] = [];
dictCart['totalSaleAmount'] = 0;



log("dictCart: " + JSON.stringify(dictCart));

/**
 * Add_lineItem
 * adds kwargs values to dictCart object
 * @param {object} kwargs 
 * @param {object} dictCart 
 */
function Add_lineItem(kwargs, dictCart) {
    var lineDict = { "subCategory": "57000", "sku": "77105811570", "quantity": 1, "originalSaleAmount": 1000, "storeCoupon": 0, "itemDiscount": 0, "mnfCoupon": 0, "promoCoupon": 0, "itemTax": 130, "saleAmount": 1000, "finalSaleAmount": 1130, "tags": '["GWP DISCOUNT"]' }
    for (property in kwargs) {
        var value = kwargs[property];
        //log(property+" = "+value);
        if (property == "subCategory") 
        {
            lineDict['subCategory'] = value;
        }
      else if (property == "sku") 
      {
            lineDict['sku'] = value;}
           else if (property == "quantity") 
        {
            lineDict['quantity'] = value;
        } 
      else {
            lineDict[property] = parseInt(value);
        }
    }
      log( lineDict['subCategory'] );
    dictCart['saleLineItems'].push(lineDict)
    dictCart['totalSaleAmount'] += (lineDict['finalSaleAmount'] + lineDict['itemTax'])
};


Add_lineItem(kwargs, dictCart);

log("\n\ndictCart After Add_lineItem: \n" + JSON.stringify(dictCart));
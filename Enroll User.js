const $host = "https://api2-us-east-1.stage.exchangesolutions.com/qa-mammoth-cb";
const getToken = true;

CB_OAUTH_ENABLED = true;
CB_CLIENT_ID = "31lc4jblltndal7md79ih7chvr";
CB_CLIENT_SECRET = "1ph1clgl0iucqj9ufgtlhhr29f827rj73kpjuvbk8b7i4d7b8825";
CB_TOKEN_URL = "https://wizard-dev.auth.us-east-1.amazoncognito.com/oauth2/token";

var isoDate = new Date().toISOString();

function processJSON(postCall) {
    var resp = process(postCall);
    return JSON.parse(resp.getDataString());
};

function externalID1(){
    var dt = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (dt + Math.random()*16)%16 | 0;
        dt = Math.floor(dt/16);
        return (c=='x' ? r :(r&0x3|0x8)).toString(16);
    });
    return uuid;
}
const externalId = externalID1();

function setGlobalBearerToken() {
    const AUTHORIZATION = "Basic MzFsYzRqYmxsdG5kYWw3bWQ3OWloN2NodnI6MXBoMWNsZ2wwaXVjcWo5dWZndGxoaHIyOWY4Mjdyajcza3BqdXZiazhiN2k0ZDdiODgyNQ==";
    var data = formData();
    data.put("grant_type", "client_credentials");
    var call = createPost(CB_TOKEN_URL, data);
    call.addHeader("Authorization", AUTHORIZATION);
    var json = processJSON(call);
    CB_TOKEN = json.access_token;
    addGlobalHeader("Authorization", "Bearer " + CB_TOKEN);
};



function enrolluser(externalId) {
   var data = {
	"externalIdentifier": externalId,
	"dateOfBirth": "1975-09-19",
	"gender": "M",
	"firstName": "Janson",
	"lastName": "Lee",
	"email": "jansonlee@esmail.com",
	"postalCode": "M1S 3C5",
    "channel": "APP"

};
    var call = createPost($host + "/client/users/", data);
    call.addHeader("MCK-CORRELATIONID", externalId);
    call.addHeader("MCK-MSGTIMESTAMP", isoDate);
    call.addHeader("MCK-APPID", "CB-WZMOB-1");
    var resp = process(call);
    var json = JSON.parse(resp.getDataString());
    locker.put("Account ID" , json.accountID);
  	locker.put("loyalty ID" , json.loyaltyCard);
  	log("Account ID : " + json.accountID);
  	log("loyalty ID : " + json.loyaltyCard);

};

setGlobalBearerToken();
enrolluser(externalId);
var Login =  function(URL,User,Password)
{
var result;
try{
  navigateTo(URL);
  setValue(textbox("username"),User);
  setValue(password("password"),Password);
  click(submit("button"));
  assertExists(span("landing-card-container-top-headline"));
  result = "Login successful";
}
catch(err)
  {
    result = "Login Failure ->Reason = "+ err.toString();
  }
finally
  {
    result = "Login Success ->Reason = "+ result;
  }
return result;
}